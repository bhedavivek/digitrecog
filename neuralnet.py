import cPickle
import gzip
import numpy as np
import random
import math
from scipy.special import expit
from os.path import exists
from scipy import ndimage
from scipy.misc import imresize

def one_hot(t):
	one_hot_t = np.zeros((len(t),10))
	for i in range(len(t)):
		one_hot_t[i][t[i]]=1
	return one_hot_t

def logistic_activation(inp):
	return 1/(1+math.exp(-inp))

def tanh_activation(inp):
	return 2/(1+ math.exp(-2*inp))

def relu_activation(inp):
	result = []
	for each in inp[0]:
		if each<=0:
			result.append(0)
		else:
			result.append(each)
	print np.shape(np.array(result))
	return np.array((1,result))
def deri_relu_activation(inp):
	if(inp>0):
		return 1
	return 0

def calc_exp_aj(x_vector, w_vector, bias):
	return math.exp(np.dot(x_vector,w_vector)+bias)

def calc_exp_a(x_vector, w_matrix, bias_matrix):
	result=0
	for i in range(10):
		result+=calc_exp_aj(x_vector,w_matrix[i],bias_matrix[i])
	return result

def calc_yj(x_vector, w_matrix, j, bias_matrix):
	return calc_exp_aj(x_vector, w_matrix[j], bias_matrix[j])/calc_exp_a(x_vector, w_matrix, bias_matrix)

#Logistic Regression using Stochastic Gradient Descent
def logistic_train(training_x, training_y, w, b, eta):
	num_samples = len(training_x)
	random_index = np.random.permutation(num_samples)
	training_x = training_x[random_index]
	training_y = training_y[random_index]
	min_err = 0.5
	sum_err=0
	iteration=0
	weight_sum=np.zeros((10,784))
	min_w=w
	for i in range(num_samples):
		cross_entropy_error = 0
		for j in range(num_classes):
			yj = calc_yj(training_x[i], w, j, b)
			cross_entropy_error += training_y[i][j]*math.log(yj)
			weight_sum[j] = weight_sum[j] + np.dot((yj-training_y[i][j]),training_x[i])
		cross_entropy_error= math.fabs(cross_entropy_error)
		sum_err +=cross_entropy_error
		n=100
		if i%n==0:
			iteration+=1
			sum_err=sum_err/n
			if iteration>1:
				if sum_err<min_err:
					eta = eta*(sum_err/min_err)
					min_err = sum_err
					min_w = w
				if sum_err<0.18:
					return w
				w = w - eta*weight_sum
			weight_sum=np.zeros((10,784))
			sum_err=0
	print 'stoch fail'
	return min_w

def neural_net_train(x_matrix,target_label, num_hidden_features, learning_rate, num_classes, batch_size, num_passes):
	num_samples = len(x_matrix)
	x_matrix = np.c_[np.ones(num_samples),x_matrix]
	num_features = len(x_matrix[0])
	np.random.seed(0)
	weights_layer_1 = np.random.randn(num_features, num_hidden_features)/np.sqrt(num_features)
	weights_layer_2 = np.random.randn(num_hidden_features,num_classes)/np.sqrt(num_classes)
	bias_1 = np.zeros((1,num_hidden_features))
	bias_2 = np.zeros((1,num_classes))
	break_point=0.022
	min_err=1
	for i in range(num_passes+1):
		j=0
		np.random.seed(0)
		random_index = np.random.permutation(len(x_matrix))
		x_matrix = x_matrix[random_index]
		target_label = target_label[random_index]
		while(j<len(x_matrix)):
			#Forward
			z1 = np.dot(x_matrix[j:j+batch_size], weights_layer_1) + bias_1
			a1 = np.tanh(z1)
			z2 = np.dot(a1,weights_layer_2) + bias_2
			exp_a2 = np.exp(z2)
			yk = exp_a2/np.sum(exp_a2, axis=1, keepdims=True)
			y=one_hot(target_label[j:j+batch_size])
			#Calculating Cross Entropy Error
			cross_entropy_error=[]
			for l in range(batch_size):
				temp=0
				for m in range(10):

					temp=y[l][m]*math.log(yk[l][m])
				cross_entropy_error.append(-temp)
			cross_entropy_error = np.mean(cross_entropy_error)
			'''if(cross_entropy_error<break_point and cross_entropy_error>0.01):
				print 'broke'
				break_point=cross_entropy_error
				break'''
			if cross_entropy_error<min_err and cross_entropy_error>0:
				min_err = cross_entropy_error

			#Back Propogation
			delta3 = yk
			delta3[range(batch_size),target_label[j:j+batch_size]]-=1
			#delta3[range(batch_size),target_label[j:j+batch_size]]*=-1
			delta_weight2 = np.dot(a1.T,delta3)
			delta2 = np.dot(delta3, weights_layer_2.T)*(1-np.power(a1,2))
			delta_weight1 = np.dot(x_matrix[j:j+batch_size].T, delta2)

			#To Prevent Overfitting
			delta_weight1+= weights_layer_1*learning_rate
			delta_weight2+=weights_layer_2*learning_rate

			#Updating Weights
			weights_layer_2 += -learning_rate*delta_weight2
			weights_layer_1 += -learning_rate*delta_weight1
			cross_entropy_error=0
			j+=batch_size
		y = neural_net_find_opt(x_matrix[:,1:],[weights_layer_1,weights_layer_2],[bias_1,bias_2])
		newErr = err(target_label,y)
		if i==0:
			min_err = newErr
		if min_err<newErr:
			min_err = newErr
			min_w1, min_w2 = weights_layer_1,weights_layer_2
		print 'Training Step',i,'Accuracy:', newErr
	return [[min_w1,min_w2],[bias_1,bias_2]]

def neural_net_find_opt(x_matrix,w_matrix_vector, bias_matrix_vector):
	x_matrix = np.c_[np.ones(len(x_matrix)),x_matrix]
	temp = x_matrix
	for i in range(len(w_matrix_vector)):
		temp=np.dot(temp,w_matrix_vector[i])+bias_matrix_vector[i]
	return np.argmax(temp, axis=1)

def find_opt(x_matrix,w_matrix):
	result = []
	for x_vector in x_matrix:
		result.append(np.argmax(np.dot(w,x_vector)))
	return np.array(result)
def err(y_exp,y_act):
	return np.mean(np.equal(y_exp,y_act))

#loading mnist data
filename = 'mnist.pkl.gz'
f = gzip.open(filename, 'rb')
training_data, validation_data, test_data = cPickle.load(f)
f.close()

training_x=training_data[0]
#Converting Target Value into One Hot vector
training_y=training_data[1]

#training_x = np.append(training_x,validation_data[0],axis=0)
#training_y = np.append(training_y,validation_data[1],axis=0)
num_classes=10

print '----- SINGLE LAYER NEURAL NETWORK WITH BACKPROPOGATION-----'
#TRAINING STARTED
w,b = neural_net_train(training_x,training_y,num_hidden_features=100, learning_rate = 0.0001, num_classes = num_classes, batch_size=10, num_passes=10)

y = neural_net_find_opt(test_data[0],w,b)
print 'Test Set Accuracy:',err(test_data[1],y)
y = neural_net_find_opt(validation_data[0],w,b)
print 'Validation Set Accuracy:',err(validation_data[1],y)
print 'done'

path = 'USPSdata/Numerals/'
final = np.empty((1,784))
count =0;
absCount=0
for number in range(10):
	for sample_number in range(1,676):
		for sample_code in ['a','b','c']:
			actual_path = path +'/'+str(number)+'/'+str(sample_number).zfill(4) + sample_code + '.png'
			if exists(actual_path):
				temp = ndimage.imread(actual_path, mode='L')
				temp = 1-(imresize(temp,(28,28), mode='L')/255)
				temp = temp.flatten()
				temp = temp.reshape(1,len(temp))
				y = neural_net_find_opt(temp,w,b)
				if y[0]==number:
					count+=1
					absCount+=1
				else:
					absCount+=1
print 'USPS DataSet Accuracy :',float(count)/float(absCount)
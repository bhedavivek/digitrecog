print "Project was Implemented using Numpy version - 1.11.2 & Scipy version - 0.18.1"
print "CNN Implementation in cnn.py"
print "This file requires Scipy Pillow library to run"
print

#Importing required libraries
import cPickle
import gzip
import numpy as np
import random
import math
from os.path import exists
from scipy import ndimage
from scipy.misc import imresize

def one_hot(t):
	one_hot_t = np.zeros((len(t),10))
	for i in range(len(t)):
		one_hot_t[i][t[i]]=1
	return one_hot_t

num_classes=10

def logistic_activation(inp):
	return 1/(1+math.exp(-inp))

def tanh_activation(inp):
	return 2/(1+ math.exp(-2*inp))

def relu_activation(inp):
	if(inp>0):
		return inp
	return 0

def calc_exp_aj(x_vector, w_vector, bias):
	return math.exp(np.dot(x_vector,w_vector)+bias)

def calc_exp_a(x_vector, w_matrix, bias_matrix):
	result=0
	for i in range(10):
		result+=calc_exp_aj(x_vector,w_matrix[i],bias_matrix[i])
	return result

def calc_yj(a_vector):
	exp = np.exp(a_vector)
	return exp/np.sum(exp)

#Logistic Regression using Stochastic Gradient Descent
def logistic_train(training_x, training_y, training_labels,w, b, eta, num_passes):
	#Creating Copy of Original Matrix
	train_x = training_x
	
	num_samples = len(training_x)
	
	#Intializing Input Matrix
	training_x = training_x.reshape(num_samples,784)

	#Appending 1 to each input matrix row
	training_x = np.c_[np.ones(num_samples),training_x]
	min_err = 3
	sum_err=[]
	iteration=0
	weight_sum=np.zeros((10,785))
	min_w=w
	break_err=0.2 #Initial Error Threshold
	for passes in range(num_passes):
		random_index = np.random.permutation(num_samples)
		training_x = training_x[random_index]
		training_y = training_y[random_index]
		for i in range(num_samples):
			cross_entropy_error = 0
			a_ = np.dot(training_x[i],w.T)+b
			y_ = calc_yj(a_)
			weight_sum += np.dot((y_ - training_y[i])[:,None], training_x[i][None,:])
			cross_entropy_error = training_y[i].dot(np.log(y_))
			cross_entropy_error*=-1
			cross_entropy_error= math.fabs(cross_entropy_error)
			sum_err.append(cross_entropy_error)
			n=100
			if i%n==0:
				iteration+=1
				sum_err=np.mean(sum_err)
				if iteration>1:
					if sum_err<min_err and sum_err>0:
						eta = eta*(sum_err/min_err)
						min_err = sum_err
						min_w = w
					#Stochastic Break Condition
					if sum_err<break_err and sum_err>0:
						break_err=sum_err
						min_w = w
						sum_err=[]
						weight_sum=np.zeros((10,785))
						break
					w = w - eta*weight_sum
				weight_sum=np.zeros((10,785))
				sum_err=[]
		y_act = find_opt(train_x,w)
		print 'Training Step '+str(passes+1)+' Accuracy',err(training_labels,y_act)
	return w

#Neural Network Training
def neural_net_train(x_matrix,target_label, num_hidden_features, learning_rate, num_classes, batch_size, num_passes):
	num_samples = len(x_matrix)

	#Intializing Input Matrix
	x_matrix = np.c_[np.ones(num_samples),x_matrix]
	num_features = len(x_matrix[0])
	
	#Intializing Weight Matrix for Hidden Layer and Output Layer
	np.random.seed(0)
	weights_layer_1 = np.random.randn(num_features, num_hidden_features)/np.sqrt(num_features)
	weights_layer_2 = np.random.randn(num_hidden_features,num_classes)/np.sqrt(num_classes)
	
	#Initializing Bias vectors for Hidden Layer and Output Layer
	bias_1 = np.zeros((1,num_hidden_features))
	bias_2 = np.zeros((1,num_classes))
	
	#Intial Break condition
	break_point=0.022
	min_err=1
	
	for i in range(num_passes+1):
		j=0
		#Random Shuffling Input Matrix and Target Vector
		np.random.seed(0)
		random_index = np.random.permutation(len(x_matrix))
		x_matrix = x_matrix[random_index]
		target_label = target_label[random_index]
		
		#Batch Learning
		while(j<len(x_matrix)):
			#Forward Propogation
			z1 = np.dot(x_matrix[j:j+batch_size], weights_layer_1) + bias_1
			a1 = np.tanh(z1)
			z2 = np.dot(a1,weights_layer_2) + bias_2
			exp_a2 = np.exp(z2)
			yk = exp_a2/np.sum(exp_a2, axis=1, keepdims=True)
			y=one_hot(target_label[j:j+batch_size])
			#Calculating Cross Entropy Error
			cross_entropy_error=[]
			for l in range(batch_size):
				temp=0
				for m in range(10):
					temp=y[l][m]*math.log(yk[l][m])
				cross_entropy_error.append(-temp)
			cross_entropy_error = np.mean(cross_entropy_error)
			
			if(cross_entropy_error<break_point and cross_entropy_error>0.01):
				break_point=cross_entropy_error
				break
			if cross_entropy_error<min_err and cross_entropy_error>0:
				min_err = cross_entropy_error

			#Back Propogation
			delta3 = yk
			delta3[range(batch_size),target_label[j:j+batch_size]]-=1
			#delta3[range(batch_size),target_label[j:j+batch_size]]*=-1
			delta_weight2 = np.dot(a1.T,delta3)
			delta2 = np.dot(delta3, weights_layer_2.T)*(1-np.power(a1,2))
			delta_weight1 = np.dot(x_matrix[j:j+batch_size].T, delta2)

			#To Prevent Overfitting
			delta_weight1+= weights_layer_1*learning_rate
			delta_weight2+=weights_layer_2*learning_rate

			#Updating Weights
			weights_layer_2 += -learning_rate*delta_weight2
			weights_layer_1 += -learning_rate*delta_weight1
			
			cross_entropy_error=0
			j+=batch_size
		
		y = neural_net_find_opt(x_matrix[:,1:],[weights_layer_1,weights_layer_2],[bias_1,bias_2])
		newErr = err(target_label,y)
		if i==0:
			min_err = newErr
		if min_err<newErr:
			min_err = newErr
			min_w1, min_w2 = weights_layer_1,weights_layer_2
		print 'Training Step',i+1,'Accuracy:', newErr
	return [[min_w1,min_w2],[bias_1,bias_2]]


def find_opt(x_matrix,w_matrix):
	x_matrix = np.c_[np.ones(len(x_matrix)),x_matrix]
	result = []
	result = np.argmax(np.dot(x_matrix,w_matrix.T),axis=1)
	return result

def find_opt_x(x_vector, w_matrix):
	x_vector = np.append(np.array([1]),x_vector)
	return np.argmax(x_vector.reshape(1,785).dot(w_matrix.T),axis=1)

#Gives Accuracy, Not Error Rate
def err(y_exp,y_act):
	return np.mean(np.equal(y_exp,y_act))

def neural_net_find_opt(x_matrix,w_matrix_vector, bias_matrix_vector):
	x_matrix = np.c_[np.ones(len(x_matrix)),x_matrix]
	temp = x_matrix
	for i in range(len(w_matrix_vector)):
		temp=np.dot(temp,w_matrix_vector[i])+bias_matrix_vector[i]
	return np.argmax(temp, axis=1)

print '----- LOGISTIC REGRESSION-----'

#loading mnist data
filename = 'mnist.pkl.gz'

f = gzip.open(filename, 'rb')
training_data, validation_data, test_data = cPickle.load(f)
f.close()

training_x=training_data[0]
#Converting Target Value into One Hot vector
training_y=one_hot(training_data[1])


#Initializing Weight and Bias Values
w=np.zeros((10,785))
b=np.zeros(10)+0.2

w=logistic_train(training_x,training_y,training_data[1],w,b, eta=0.01, num_passes = 10)

y_act = find_opt(validation_data[0],w)
print 'Validation Accuracy',err(validation_data[1],y_act)

y_act = find_opt(test_data[0],w)
print 'Test Accuracy',err(test_data[1],y_act)

#Loading USPS Dataset
path = 'USPSdata/Test/'
final = np.empty((1,784))
count =0;
absCount=0
usps_x=[]
usps_y=[]
for number in range(1,1501):
	actual_path = path +'/'+'test_'+str(number).zfill(4)+ '.png'
	label = 9 - int(number/150)
	if (float(number)/150)%1==0:
		label+=1
	if exists(actual_path):
		temp = ndimage.imread(actual_path, mode='L')
		temp = 1-imresize(temp,(28,28), mode='L')/255
		temp = temp.flatten()
		usps_x.append(temp)
		usps_y.append(label)
y_act = find_opt(usps_x,w)
print 'USPS Dataset Accuracy:',err(usps_y,y_act)

print

print '----- SINGLE LAYER NEURAL NETWORK WITH BACKPROPOGATION-----'

#TRAINING STARTED
w,b = neural_net_train(training_x,training_data[1],num_hidden_features=100, learning_rate = 0.0001, num_classes = num_classes, batch_size=50, num_passes=50)
y = neural_net_find_opt(test_data[0],w,b)
print 'Test Set Accuracy:',err(test_data[1],y)
y = neural_net_find_opt(validation_data[0],w,b)
print 'Validation Set Accuracy:',err(validation_data[1],y)
y_act = find_opt(usps_x,w)
print 'USPS Dataset Accuracy:',err(usps_y,y_act)